
# AFP - Dominik Dosoudil

## Téma projektu

Projekt je zaměřený na jednoduché přidělování osob k projektům.
Zároveň je možné osobám přidávat skilly, což zjednodušuje project managerovi výběr lidí, které na projekt vezme.

## Zadání semestrální práce

Tato práce původně vznikla v předmětu BI-TJV, kde bylo za úkol vytvořit REST API.
Toto REST API jsme měli nějakým způsobem vyzkoušet a předvést na obhajobě.
Z toho důvodu jsem využil volnosti výběru clienta a rozhodl se naučit a vyzkoušet elm.
Proto bych v této semestrální práci rád navázal.

1. refactoring a přepsání částí aplikace na základě nových znalostí z NI-AFP
2. přidání české a anglické lokalizace pomocí [ChristophP/elm-i18next](https://package.elm-lang.org/packages/ChristophP/elm-i18next/latest/)
   1. případně generování funkcí pro lokalizace pomocí [elm-i18n-gen](https://www.npmjs.com/package/elm-i18n-gen?activeTab=readme)
3. deployment aplikace (produkční build a nasazení na veřejně dostupný server)
