module Input.MultiSelect exposing (..)

-- VIEW

import Constants exposing (checkMarkCode)
import Element exposing (Element, column, row, spacing, text)
import Element.Events exposing (onClick)
import Utils exposing (codeToString)


type alias Option a msg =
    { text : Element msg
    , value : a
    }


type alias Config a msg =
    { options : List (Option a msg)
    , values : List a
    , handleClick : a -> msg
    }


checkMarkIcon =
    codeToString checkMarkCode


multiSelect : Config a msg -> Element msg
multiSelect { options, values, handleClick } =
    column [ spacing 5 ]
        (List.map
            (\opt ->
                row [ spacing 10, onClick (handleClick opt.value) ]
                    [ text
                        (if List.member opt.value values then
                            checkMarkIcon

                         else
                            " "
                        )
                    , opt.text
                    ]
            )
            options
        )
