module Constants exposing (..)


apiUrl =
    "https://afpbe-201f9-8080.app.zerops.io/"


magnifyingGlassCode =
    128269


crossCode =
    10007


checkMarkCode =
    10003
