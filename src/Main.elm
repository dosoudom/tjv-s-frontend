module Main exposing (..)

import Browser
import Browser.Navigation as Nav
import Element exposing (Attr, Element, column, htmlAttribute, layout, padding, spacing)
import Framework.Button as Button exposing (buttonLink)
import Framework.Modifier as Modifier exposing (..)
import Framework.Typography as Typography
import Html exposing (..)
import Html.Attributes
import Page.Person exposing (Msg(..), Person, Skills(..))
import Page.Project
import Page.Skill
import Route exposing (Route, fromUrl)
import Translations
import Url exposing (Protocol(..))
import Utils


type alias Document msg =
    { title : String
    , body : List (Html msg)
    }


{-| js interop, flags that are passed to elm app from javascript
-}
type alias Flags =
    ()


{-| following type is required so elm knows what flags will be passed to elm program
elm performs type validation (js to elm) of flags when instantiating the application
<https://guide.elm-lang.org/interop/flags.html>
-}
main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }



-- INIT


{-| type of application state -
-}
type alias Model =
    { route : Route
    , key : Nav.Key
    , personModel : Page.Person.Model
    , skillModel : Page.Skill.Model
    , projectModel : Page.Project.Model
    , lang : Translations.Lang
    }


init : Flags -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url key =
    Route.fromUrl url
        |> (\route ->
                ( Model
                    route
                    key
                    Page.Person.init
                    Page.Skill.init
                    Page.Project.init
                    Translations.En
                , fetchByRoute route
                )
           )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- UPDATE


{-| type of messages that can be fired in the application
-}
type Msg
    = NoOp
    | LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | Refresh
    | PersonMsg Page.Person.Msg
    | SkillMsg Page.Skill.Msg
    | ProjectMsg Page.Project.Msg
    | ChangeLang Translations.Lang


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            fromUrl url |> (\route -> ( { model | route = route }, fetchByRoute route ))

        Refresh ->
            ( model, fetchByRoute model.route )

        PersonMsg personMsg ->
            let
                ( personModel, cmd ) =
                    Page.Person.update personMsg model.personModel
            in
            ( { model | personModel = personModel }, Cmd.map PersonMsg cmd )

        SkillMsg skillMsg ->
            let
                ( skillModel, cmd ) =
                    Page.Skill.update skillMsg model.skillModel
            in
            ( { model | skillModel = skillModel }, Cmd.map SkillMsg cmd )

        ProjectMsg projectMsg ->
            let
                ( projectModel, cmd ) =
                    Page.Project.update projectMsg model.projectModel
            in
            ( { model | projectModel = projectModel }, Cmd.map ProjectMsg cmd )

        ChangeLang lang ->
            ( { model | lang = lang }, Cmd.none )



-- VIEW


view : Model -> Document Msg
view model =
    Document (Translations.teamSkills model.lang)
        [ div []
            [ layout
                []
                (column [ padding 20, spacing 10 ]
                    [ Element.row [ spacing 20 ]
                        [ langSwitch model.lang
                        , Typography.h1 [] (Element.text (Translations.teamSkills model.lang))
                        ]
                    , Button.button [ Modifier.Medium, Success, Outlined ] (Just Refresh) (Translations.refresh model.lang)
                    , viewNav model.lang
                    , viewRouter model
                    ]
                )
            ]
        ]


viewNav lang =
    Element.row
        [ spacing 100 ]
        [ buttonLink [ Medium ] (Route.buildUrl (Route.Person Route.List)) (Translations.persons lang)
        , buttonLink [ Medium ] (Route.buildUrl (Route.Skill Route.List)) (Translations.skills lang)
        , buttonLink [ Medium ] (Route.buildUrl (Route.Project Route.List)) (Translations.projects lang)
        ]


view404 lang =
    Element.el [] (Typography.h2 [] (Element.text (Translations.notFound404 lang)))


langSwitch lang =
    let
        grayScaleValue l =
            if lang == l then
                "0"

            else
                "1"

        flagStyle l =
            Html.Attributes.style "filter" ("grayscale(" ++ grayScaleValue l ++ ")")

        flagButton l =
            Element.el [ htmlAttribute (flagStyle l) ] <|
                Button.button [] (Just <| ChangeLang l) <|
                    Utils.flagIcon l
    in
    Element.row [ spacing 10 ]
        [ flagButton Translations.Cz
        , flagButton Translations.En
        ]


viewRouter : Model -> Element Msg
viewRouter model =
    case model.route of
        Route.Person crud ->
            Element.map PersonMsg (Page.Person.view crud model.personModel model.lang)

        Route.Skill crud ->
            Element.map SkillMsg (Page.Skill.view crud model.skillModel model.lang)

        Route.Project crud ->
            Element.map ProjectMsg (Page.Project.view crud model.projectModel model.lang)

        _ ->
            view404 model.lang



-- HTTP


fetchByRoute : Route -> Cmd Msg
fetchByRoute route =
    case route of
        Route.Person crud ->
            Cmd.map PersonMsg (Page.Person.fetchByRoute crud)

        Route.Skill crud ->
            Cmd.map SkillMsg (Page.Skill.fetchByRoute crud)

        Route.Project crud ->
            Cmd.map ProjectMsg (Page.Project.fetchByRoute crud)

        _ ->
            Cmd.none
