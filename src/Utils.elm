module Utils exposing (..)

-- I think that it's fine to expose everything from Utils module since the module is meant to include trivial functions used all over the project

import Framework.Color
import Framework.Spinner
import Http
import Translations



-- VIEW


codeToString : Int -> String
codeToString =
    Char.fromCode
        >> String.fromChar


flagIcon : Translations.Lang -> String
flagIcon lang =
    case lang of
        Translations.En ->
            codeToString 127468 ++ codeToString 127463

        Translations.Cz ->
            codeToString 127464 ++ codeToString 127487


viewLoading =
    Framework.Spinner.spinner Framework.Spinner.Rotation 32 Framework.Color.black



-- HTTP


{-| syntax sugar for basic http DELETE requests
-}
httpDelete :
    { url : String
    , expect : Http.Expect msg
    }
    -> Cmd msg
httpDelete { url, expect } =
    Http.request
        { method = "DELETE"
        , headers = [ Http.header "Accept" "*/*" ]
        , url = url
        , body = Http.emptyBody
        , expect = expect
        , timeout = Nothing
        , tracker = Nothing
        }


{-| syntax sugar for http PUT requests
-}
httpPut :
    { url : String
    , body : Http.Body
    , expect : Http.Expect msg
    }
    -> Cmd msg
httpPut { url, body, expect } =
    Http.request
        { method = "PUT"
        , headers = [ Http.header "Accept" "*/*" ]
        , url = url
        , body = body
        , expect = expect
        , timeout = Nothing
        , tracker = Nothing
        }
