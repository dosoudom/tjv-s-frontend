module Page.Skill exposing (Model, Msg, Skill, fetchByRoute, getSkills, init, skillDecoder, update, view)

import Constants exposing (apiUrl, crossCode, magnifyingGlassCode)
import Element exposing (Element, column, row, shrink, spacing, table, text)
import Element.Input as Input
import Framework.Button exposing (button, buttonLink)
import Framework.FormField exposing (inputText)
import Framework.Modifier exposing (Modifier(..))
import Framework.Typography exposing (h2)
import Http
import Json.Decode exposing (Decoder, field, int, list, map2, string)
import Json.Encode as Encode
import Route
import Translations
import Utils exposing (codeToString, httpDelete, httpPut, viewLoading)



-- INIT


type EditMode
    = Enable
    | Disable
    | Save


type FormField
    = Name


type alias Form =
    { name : String }


initForm : Maybe Skill -> Form
initForm =
    Maybe.map (\skill -> { name = skill.name })
        >> Maybe.withDefault { name = "" }


type alias Model =
    { skills : List Skill
    , skill : Maybe Skill
    , form : Form
    , editing : Bool
    }


init =
    Model
        []
        Nothing
        (initForm Nothing)
        False



-- UPDATE


type Msg
    = NoOp
    | GotSkills (Result Http.Error (List Skill))
    | GotSkill (Result Http.Error Skill)
    | DeleteRequest Int
    | SkillDeleted Int (Result Http.Error ())
    | SkillCreated (Result Http.Error Skill)
    | FormChange FormField String
    | FormSubmit
    | Edit EditMode


updateForm : FormField -> String -> Form -> Form
updateForm field value form =
    case field of
        Name ->
            { form | name = value }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        GotSkills result ->
            case result of
                Ok skills ->
                    ( { model | skills = skills }, Cmd.none )

                Err error ->
                    ( model, Cmd.none )

        GotSkill result ->
            ( { model
                | skill =
                    case result of
                        Ok skill ->
                            Just skill

                        Err _ ->
                            Nothing
              }
            , Cmd.none
            )

        DeleteRequest id ->
            ( model, deleteSkill id )

        SkillDeleted id result ->
            case result of
                Ok () ->
                    ( model, Route.redirect (Route.Skill Route.List) )

                Err error ->
                    ( model, Cmd.none )

        SkillCreated result ->
            case result of
                Ok _ ->
                    ( { model | form = initForm Nothing }, Route.redirect (Route.Skill Route.List) )

                Err _ ->
                    ( model, Cmd.none )

        FormChange formField value ->
            case formField of
                Name ->
                    ( { model | form = updateForm formField value model.form }, Cmd.none )

        FormSubmit ->
            ( model, createSkill model.form )

        Edit Enable ->
            ( { model | editing = True, form = initForm model.skill }, Cmd.none )

        Edit Disable ->
            ( { model | editing = False, form = initForm Nothing }, Cmd.none )

        Edit Save ->
            case model.skill of
                Just skill ->
                    ( { model | editing = False, form = initForm Nothing }, editSkill skill.id model.form GotSkill )

                Nothing ->
                    ( model, Cmd.none )



-- VIEW


view : Route.CRUD -> Model -> Translations.Lang -> Element Msg
view crud model lang =
    case crud of
        Route.List ->
            column [ spacing 10 ]
                [ h2 [] (text (Translations.skills lang))
                , table [ spacing 10 ]
                    { data = model.skills
                    , columns =
                        [ { header = text (Translations.id lang)
                          , width = shrink
                          , view = .id >> String.fromInt >> text
                          }
                        , { header = text (Translations.name lang)
                          , width = shrink
                          , view = .name >> text
                          }
                        , { header = text (Translations.detail lang)
                          , width = shrink
                          , view = \s -> buttonLink [] (Route.buildUrl (Route.Skill (Route.Get s.id))) (codeToString magnifyingGlassCode)
                          }
                        , { header = text (Translations.delete lang)
                          , width = shrink
                          , view = \s -> button [] (Just (DeleteRequest s.id)) (codeToString crossCode)
                          }
                        ]
                    }
                , buttonLink [ Medium, Success, Outlined ] (Route.buildUrl (Route.Skill Route.Add)) "+"
                ]

        Route.Get id ->
            column [ spacing 20 ]
                (case model.skill of
                    Just skill ->
                        case model.editing of
                            False ->
                                [ row [ spacing 20 ] [ h2 [] (text (Translations.skill lang)), button [] (Just (Edit Enable)) (Translations.edit lang) ]
                                , text (String.fromInt skill.id)
                                , text skill.name
                                ]

                            True ->
                                [ viewForm model.form lang
                                , button [] (Just (Edit Save)) (Translations.save lang)
                                ]

                    Nothing ->
                        [ viewLoading ]
                )

        Route.Add ->
            column [ spacing 20 ]
                [ h2 [] (text (Translations.addEntity lang (Translations.skill lang)))
                , viewForm model.form lang
                , button [ Medium, Outlined, Success ] (Just FormSubmit) (Translations.submit lang)
                ]


viewForm : Form -> Translations.Lang -> Element Msg
viewForm form lang =
    inputText []
        { field = Name
        , label = text (Translations.name lang)
        , fieldValue = form.name
        , inputType = Input.text
        , maybeFieldFocused = Nothing
        , inputTypeAttrs = []
        , msgOnChange = FormChange
        , msgOnFocus = \_ -> NoOp
        , msgOnLoseFocus = \_ -> NoOp
        , maybeMsgOnEnter = Nothing
        , helperText = Nothing
        }



-- HTTP


fetchByRoute : Route.CRUD -> Cmd Msg
fetchByRoute crud =
    case crud of
        Route.Get id ->
            getSkill id GotSkill

        Route.List ->
            getSkills GotSkills

        Route.Add ->
            Cmd.map (always NoOp) Cmd.none


getSkills : (Result Http.Error (List Skill) -> msg) -> Cmd msg
getSkills msg =
    Http.get
        { url = apiUrl ++ "skills"
        , expect = Http.expectJson msg (list skillDecoder)
        }


getSkill : Int -> (Result Http.Error Skill -> msg) -> Cmd msg
getSkill id msg =
    Http.get
        { url = apiUrl ++ "skills/" ++ String.fromInt id
        , expect = Http.expectJson msg skillDecoder
        }


createSkill : Form -> Cmd Msg
createSkill form =
    Http.post
        { url = apiUrl ++ "skills"
        , body = Http.jsonBody (Encode.string form.name)
        , expect = Http.expectJson SkillCreated skillDecoder
        }


deleteSkill : Int -> Cmd Msg
deleteSkill id =
    httpDelete
        { url = apiUrl ++ "skills/" ++ String.fromInt id
        , expect = Http.expectWhatever (SkillDeleted id)
        }


editSkill : Int -> Form -> (Result Http.Error Skill -> msg) -> Cmd msg
editSkill id form msg =
    httpPut
        { url = apiUrl ++ "skills/" ++ String.fromInt id
        , body = Http.jsonBody (Encode.string form.name)
        , expect = Http.expectJson msg skillDecoder
        }



-- JSON


type alias Skill =
    { id : Int
    , name : String
    }


skillDecoder : Decoder Skill
skillDecoder =
    map2 Skill
        (field "id" int)
        (field "name" string)
