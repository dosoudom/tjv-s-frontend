module Page.Project exposing (Model, Msg, fetchByRoute, init, update, view)

import Constants exposing (apiUrl, crossCode, magnifyingGlassCode)
import Element exposing (Element, column, row, shrink, spacing, table, text)
import Element.Input as Input
import Framework.Button exposing (button, buttonLink)
import Framework.FormField exposing (inputText)
import Framework.Modifier exposing (Modifier(..))
import Framework.Typography exposing (h2)
import Http
import Input.MultiSelect exposing (Option, multiSelect)
import Json.Decode exposing (Decoder, field, int, list, map4, maybe, string)
import Json.Encode as Encode
import Page.Person exposing (Person, getPersons)
import Route
import Translations
import Utils exposing (codeToString, httpDelete, httpPut, viewLoading)



-- INIT


type EditMode
    = Enable
    | Disable
    | Save


type alias Form =
    { name : String
    , manDayEstimate : Maybe Int
    , personIds : List Int
    }


initForm : Maybe Project -> Form
initForm =
    Maybe.map (\{ name, manDayEstimate, personIds } -> Form name manDayEstimate personIds)
        >> Maybe.withDefault (Form "" Nothing [])


type FormField
    = Name
    | ManDayEstimate
    | PersonIds Int


type alias Model =
    { projects : List Project
    , project : Maybe Project
    , editing : Bool
    , form : Form
    , availablePersons : List Person
    }


init : Model
init =
    Model
        []
        Nothing
        False
        (initForm Nothing)
        []



-- UPDATE


type InputType
    = Text FormField String
    | MultiSelect FormField


type Msg
    = NoOp
    | DeleteRequest Int
    | GotProject (Result Http.Error Project)
    | GotProjects (Result Http.Error (List Project))
    | ProjectDeleted Int (Result Http.Error ())
    | FormChange InputType
    | FormSubmit
    | ProjectCreated (Result Http.Error Project)
    | Edit EditMode
    | GotAvailablePersons (Result Http.Error (List Person))


updateForm : InputType -> Form -> Form
updateForm change form =
    case change of
        Text Name value ->
            { form | name = value }

        Text ManDayEstimate value ->
            case value of
                "" ->
                    { form | manDayEstimate = Nothing }

                _ ->
                    -- let the previous value when parsing fails
                    String.toInt value
                        |> Maybe.map (\manDayEstimate -> { form | manDayEstimate = Just manDayEstimate })
                        |> Maybe.withDefault form

        Text _ _ ->
            form

        MultiSelect (PersonIds id) ->
            if List.member id form.personIds then
                -- if present then remove
                { form | personIds = List.filter ((/=) id) form.personIds }

            else
                -- if not present then add
                { form | personIds = id :: form.personIds }

        MultiSelect _ ->
            form


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        GotProjects result ->
            case result of
                Ok projects ->
                    ( { model | projects = projects }, Cmd.none )

                Err error ->
                    ( model, Cmd.none )

        GotProject result ->
            case result of
                Ok project ->
                    ( { model | project = Just project }, Cmd.none )

                Err error ->
                    ( model, Cmd.none )

        DeleteRequest id ->
            ( model, deleteProject id (ProjectDeleted id) )

        ProjectDeleted id result ->
            case result of
                Ok () ->
                    ( model, Route.redirect (Route.Project Route.List) )

                Err error ->
                    ( model, Cmd.none )

        FormChange change ->
            ( { model | form = updateForm change model.form }, Cmd.none )

        FormSubmit ->
            ( { model | form = initForm Nothing }, createProject model.form ProjectCreated )

        ProjectCreated project ->
            ( model, Route.redirect (Route.Project Route.List) )

        Edit Enable ->
            ( { model | editing = True, form = initForm model.project }, getPersons GotAvailablePersons )

        Edit Disable ->
            ( { model | editing = False, form = initForm Nothing }, Cmd.none )

        Edit Save ->
            case model.project of
                Just project ->
                    ( { model | editing = False, form = initForm Nothing }, editProject project.id model.form GotProject )

                Nothing ->
                    ( model, Cmd.none )

        GotAvailablePersons result ->
            case result of
                Ok persons ->
                    ( { model | availablePersons = persons }, Cmd.none )

                Err error ->
                    ( model, Cmd.none )



-- VIEW


viewForm : Form -> List Person -> Translations.Lang -> Element Msg
viewForm form availablePersons lang =
    column [ spacing 20 ]
        [ inputText []
            { field = Name
            , label = text (Translations.name lang)
            , fieldValue = form.name
            , inputType = Input.text
            , maybeFieldFocused = Nothing
            , inputTypeAttrs = []
            , msgOnChange = \field value -> FormChange (Text field value)
            , msgOnFocus = \_ -> NoOp
            , msgOnLoseFocus = \_ -> NoOp
            , maybeMsgOnEnter = Nothing
            , helperText = Nothing
            }
        , inputText []
            { field = ManDayEstimate
            , label = text (Translations.manDayEstimate lang)
            , fieldValue =
                form.manDayEstimate
                    |> Maybe.map String.fromInt
                    |> Maybe.withDefault ""
            , inputType = Input.text
            , maybeFieldFocused = Nothing
            , inputTypeAttrs = []
            , msgOnChange = \field value -> FormChange (Text field value)
            , msgOnFocus = \_ -> NoOp
            , msgOnLoseFocus = \_ -> NoOp
            , maybeMsgOnEnter = Nothing
            , helperText = Nothing
            }
        , multiSelect
            { options = List.map (\p -> Option (text p.name) p.id) availablePersons
            , values = form.personIds
            , handleClick = PersonIds >> MultiSelect >> FormChange
            }
        ]


view : Route.CRUD -> Model -> Translations.Lang -> Element Msg
view crud model lang =
    case crud of
        Route.List ->
            column [ spacing 10 ]
                [ h2 [] (text (Translations.projects lang))
                , table [ spacing 10 ]
                    { data = model.projects
                    , columns =
                        [ { header = text (Translations.id lang)
                          , width = shrink
                          , view = .id >> String.fromInt >> text
                          }
                        , { header = text (Translations.name lang)
                          , width = shrink
                          , view = .name >> text
                          }
                        , { header = text (Translations.manDayEstimate lang)
                          , width = shrink
                          , view =
                                .manDayEstimate
                                    >> Maybe.map String.fromInt
                                    >> Maybe.withDefault "/"
                                    >> text
                          }
                        , { header = text (Translations.detail lang)
                          , width = shrink
                          , view = \s -> buttonLink [] (Route.buildUrl (Route.Project (Route.Get s.id))) (codeToString magnifyingGlassCode)
                          }
                        , { header = text (Translations.delete lang)
                          , width = shrink
                          , view = \s -> button [] (Just (DeleteRequest s.id)) (codeToString crossCode)
                          }
                        ]
                    }
                , buttonLink [ Medium, Success, Outlined ] (Route.buildUrl (Route.Project Route.Add)) "+"
                ]

        Route.Get id ->
            case model.project of
                Nothing ->
                    viewLoading

                Just project ->
                    case model.editing of
                        False ->
                            column [ spacing 20 ]
                                [ row [ spacing 20 ] [ h2 [] (text (Translations.project lang)), button [] (Just (Edit Enable)) (Translations.edit lang) ]
                                , text project.name
                                , project.manDayEstimate
                                    |> Maybe.map String.fromInt
                                    |> Maybe.withDefault "/"
                                    |> text
                                , row [ spacing 20 ]
                                    (List.map
                                        (\pId ->
                                            List.filter (.id >> (==) pId) model.availablePersons
                                                |> List.head
                                                |> Maybe.map .name
                                                |> Maybe.withDefault "loading..."
                                                |> text
                                        )
                                        project.personIds
                                    )
                                ]

                        True ->
                            column [ spacing 20 ]
                                [ row [ spacing 20 ] [ h2 [] (text (Translations.editProject lang)), button [] (Just (Edit Disable)) (Translations.cancel lang) ]
                                , viewForm model.form model.availablePersons lang
                                , button [] (Just (Edit Save)) (Translations.save lang)
                                ]

        Route.Add ->
            column [ spacing 20 ]
                [ h2 [] (text (Translations.addEntity lang (Translations.project lang)))
                , viewForm model.form model.availablePersons lang
                , button [ Medium, Outlined, Success ] (Just FormSubmit) (Translations.submit lang)
                ]



-- HTTP


fetchByRoute : Route.CRUD -> Cmd Msg
fetchByRoute crud =
    case crud of
        Route.Get int ->
            Cmd.batch [ getProject int GotProject, getPersons GotAvailablePersons ]

        Route.List ->
            getProjects GotProjects

        Route.Add ->
            getPersons GotAvailablePersons


getProjects : (Result Http.Error (List Project) -> msg) -> Cmd msg
getProjects msg =
    Http.get
        { url = apiUrl ++ "projects"
        , expect = Http.expectJson msg (list projectDecoder)
        }


getProject : Int -> (Result Http.Error Project -> msg) -> Cmd msg
getProject id msg =
    Http.get
        { url = apiUrl ++ "projects/" ++ String.fromInt id
        , expect = Http.expectJson msg projectDecoder
        }


createProject : Form -> (Result Http.Error Project -> msg) -> Cmd msg
createProject form msg =
    Http.post
        { url = apiUrl ++ "projects"
        , body = Http.jsonBody (projectEncoder form)
        , expect = Http.expectJson msg projectDecoder
        }


editProject : Int -> Form -> (Result Http.Error Project -> msg) -> Cmd msg
editProject id form msg =
    httpPut
        { url = apiUrl ++ "projects/" ++ String.fromInt id
        , body = Http.jsonBody (projectEncoder form)
        , expect = Http.expectJson msg projectDecoder
        }


deleteProject : Int -> (Result Http.Error () -> msg) -> Cmd msg
deleteProject id msg =
    httpDelete
        { url = apiUrl ++ "projects/" ++ String.fromInt id
        , expect = Http.expectWhatever msg
        }



-- JSON


type alias Project =
    { id : Int
    , name : String
    , manDayEstimate : Maybe Int
    , personIds : List Int
    }


projectDecoder : Decoder Project
projectDecoder =
    map4 Project
        (field "id" int)
        (field "name" string)
        (field "manDayEstimate" (maybe int))
        (field "personIds" (list int))


projectEncoder : Form -> Encode.Value
projectEncoder form =
    Encode.object
        [ ( "name", Encode.string form.name )
        , ( "manDayEstimate"
          , form.manDayEstimate
                |> Maybe.map Encode.int
                |> Maybe.withDefault Encode.null
          )
        , ( "personIds", Encode.list Encode.int form.personIds )
        ]
