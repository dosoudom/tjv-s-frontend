module Route exposing (CRUD(..), Route(..), buildUrl, fromUrl, redirect)

import Browser.Navigation as Nav
import Url exposing (Url)
import Url.Parser exposing ((</>), Parser, int, map, oneOf, parse, s, top)


type CRUD
    = Get Int
    | List
    | Add


type Route
    = Home
    | NotFound404
    | Person CRUD
    | Skill CRUD
    | Project CRUD


personBaseUri =
    "persons"


skillBaseUri =
    "skills"


projectBaseUri =
    "projects"


parser : Parser (Route -> a) a
parser =
    oneOf
        [ map Home top

        -- person
        , map (Get >> Person) (s personBaseUri </> int)
        , map (Person List) (s personBaseUri)
        , map (Person Add) (s personBaseUri </> s "add")

        -- skill
        , map (Get >> Skill) (s skillBaseUri </> int)
        , map (Skill List) (s skillBaseUri)
        , map (Skill Add) (s skillBaseUri </> s "add")

        -- project
        , map (Get >> Project) (s projectBaseUri </> int)
        , map (Project List) (s projectBaseUri)
        , map (Project Add) (s projectBaseUri </> s "add")
        ]


fromUrl : Url -> Route
fromUrl url =
    parse parser { url | path = Maybe.withDefault "" url.fragment, fragment = Nothing }
        |> Maybe.withDefault NotFound404


buildUrl : Route -> String
buildUrl route =
    let
        handleCrud crud =
            case crud of
                List ->
                    ""

                Get id ->
                    "/" ++ String.fromInt id

                Add ->
                    "/add"
    in
    "#"
        ++ (case route of
                Home ->
                    ""

                Person crud ->
                    "/" ++ personBaseUri ++ handleCrud crud

                Project crud ->
                    "/" ++ projectBaseUri ++ handleCrud crud

                Skill crud ->
                    "/" ++ skillBaseUri ++ handleCrud crud

                NotFound404 ->
                    -- ¯\_(ツ)_/¯
                    "/404"
           )


redirect : Route -> Cmd msg
redirect route =
    buildUrl route
        |> Nav.load
