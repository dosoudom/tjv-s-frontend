### Spuštění frontend části (client) semestrální práce:
```shell script
docker-compose build
docker-compose up
```

Nyní stačí navštívit [http://localhost](http://localhost)

# Ovládání

## Výpis záznamů z databáze se základními údaji

Na této stránce je možné mazat nebo přejít na stránku s více informacemi.

Také je možné otevřít formulář, který umožní přidat další záznam.

![list items page](./doc/img/list_items.png)

## Detail osob
![person detail](./doc/img/person_detail.png)

### přidávání skillu
![person detail add skill](./doc/img/person_detail_add_skill.png)

### úprava osob
Přidávání osob vypadá stejně, jen nejsou předvyplněné údaje.

![person edit](./doc/img/edit_person.png)

## Skill

Manipulace se skilly neobsahuje nic nového.

## Projekty

### úprava projektu

Oproti mechanismu přidávání skillů osobám se osoby k projektu přiřazují již při vytváření/editaci.
(Nemá to žádný speciální důvod, jen jsem si chtěl zkusit různé přístupy)

![project edit](./doc/img/edit_project.png)

